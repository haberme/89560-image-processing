%201508793 Haberman Eran
%
%histShape Histogram Shape.
%   the function take image as uint8 matrix and perform Histogram Shape on
%   it using a destination image also as an uint8 matrix. the function that
%   returns a new image which is the source image after the effect of the
%   Histogram Shape. the given image will be in the format uint8.
%
%   parameters:
%   srcImg - the sorce image represented by a uint8 matrix.
%   destImg - the destination image represented by a uint8 matrix.
%
%   return:
%   newImg - the srcImg after the effect of the Histogram Shape 
%   represented by a uint8 matrix.
%
%   example:
%   img1 = imread('darkimage.tiff');
%   img2 = imread('flatHistShape.tiff');
%   img3 = histShape(img1,img2);
%   imshow(img3);

function [newImg] = histShape(srcImg, destImg)
%histShape the function preform Histogram Shape on sorce and destination
%   images.
newImg = srcImg;
%get the normalized accumulated histogram of the source and destenation
%   images.
snahist = getnahist(srcImg);
dnahist = getnahist(destImg);
cv = zeros(1,256);

%compute cv according to the algorithm of Histogram Shape we saw in the 
%   tirgul.
s = 1;
d = 1;
while s <= 256
    if dnahist(d) < snahist(s)
        d = d + 1;
    else
        cv(s) = d;
        s = s + 1;
    end
end

%compute the new image based on the computed cv.
[rows, cols] = size(newImg);
for r=1:rows
    for c=1:cols
        newImg(r,c) = cv(newImg(r,c) + 1);
    end
end
end







function [nahist] = getnahist(img)
%nahistCompute compute the normalized accumulate hist on an image.
%   parameters:
%   img - the image represented by a uint8 matrix.
%
%   return:
%   nahist - the normalized accumulate hist of the image.

%Compute the hist of the image.
[rows, cols] = size(img);
hist = zeros(1,256);
for r=1:rows
    for c=1:cols
        hist(img(r,c)+1) = hist(img(r,c)+1) + 1;
    end
end

%Compute the accumulate hist of the iamge.
ahist = hist;
for i=2:256
    ahist(i) = ahist(i)+ahist(i-1);
end

%Normalize the accumulate hist of the image.
nahist = ahist/(rows*cols);
end