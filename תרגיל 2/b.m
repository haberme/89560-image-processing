clear;
clc;

i = imread('house.tiff');
i = double(i)/255;
imshow(i);

f = fft2(i);
%fftShow(f);
sf = fftshift(f);
%fftShow(sf);

sf(259, 221) = 0;
sf(275, 221) = 0;
%fftShow(sf);

isf = ifftshift(sf);
in = ifft2(isf);
%imshow(in);