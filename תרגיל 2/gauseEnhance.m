%201508793 Haberman Eran
%207424482 Kantor Adar
%
%The function gauseEnhance pass an image through gaussian nosie of mean= 0,
%   and var= 0.01 then use the following masks with the directional 
%   smoothing algorithm to neutralize the effect.
%   NOTE: while there are more masks it seems that or gaussian masks tend
%   to make the image less visible or keep the noise.
%
% mask 1 to 3:
%   two slant 3*3 matrices and one Vertical. thoes are standard matrices 
%   for directional smoothing as mentioned above no horizontal mask as it 
%   tend to blur the image.
% mask 4
%   ones(5,3) matrix. note that the matrix not only get rid of the nouse
%   but keep the image clear for example applying this mask make the people
%   more visible, also it important to note that the clouds seem more 
%   visible and less mistaken for noise as other mask tens to do.
function [eImg,nImg] = gauseEnhance(img)
% Add the gaussian noise to the image.
nImg = imnoise(img, 'gaussian', 0, 0.01);
 %Set the initial input for the directional smoothing. 
[rows, cols] = size(img);
eImg = zeros(rows, cols);

%Set the masks.
%Slant masks.
mask1 = [1,0,0;0,1,0;0,0,1] / 3;
mask2 = [0,0,1;0,1,0;1,0,0] / 3;

%Vertical masks
mask3 = ones(1,3) / 3;
mask4 = ones(5,3)/15;


%Apply the masks.
results(:,:,1) = conv2(nImg,mask1,'same');
results(:,:,2) = conv2(nImg,mask2,'same');
results(:,:,3) = conv2(nImg,mask3,'same');
results(:,:,4) = conv2(nImg,mask4,'same');

%Choose the closest result.
for r=1:rows
    for c=1:cols
        [value index] = min(abs(results(r,c,:)-nImg(r,c)));
        %Set the result to the corresponding pixal.
        eImg(r,c) = results(r,c,index);
    end
end
end