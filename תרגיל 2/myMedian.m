%201508793 Haberman Eran
%207424482 Kantor Adar
%
% The function myMedian pass the image through a median filter to get rid
% of noise. It gets an image and filter size and outputs the enhanced image.
% For each pixel the new value is the median of the adjacent color values.
% If  the filter exceeds image matrix size (image edge) the original values
% are returned.
function [newImg] = myMedian(img,rows,cols)
% Get Image size
[imRows,imCols] = size(img);
newImg = img;
% Go through all pixels whose median mask doesn't not exceed image size
for r = ceil(rows/2):(imRows-floor(rows/2))
    for c = ceil(cols/2):(imCols-floor(cols/2))
        % Create sorted block of all relevant colors
        block = sort(img((1:rows) +r-ceil(rows/2),(1:cols)+c-ceil(cols/2)));
        % Choose the median to be the current value
        newImg(r,c) = block(floor(rows*cols/2));
    end
end
end