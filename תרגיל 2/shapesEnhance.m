%201508793 Haberman Eran
%207424482 Kantor Adar
%
% This function gets an image and outputs the image with shaoed noise and
% the noised image after median enhancement. It creates to different masks
% for 'salt and for 'pepper' each with density 0.0015 and combines the
% original image with the two.
% For the enhancement myMedian is called with a size of 1x5. The size is 1
% by the short noise mask edge (3) time 2 plus 1. This size almost
% guarantees that the block size will have more original pixels than noise
% whilst not being too big and change the image too much.
function [eImg,nImg] = shapesEnhance(img)
% Create noise structure
shape = [1,0,0,0,1;0,1,0,1,0;0,0,1,0,0];
% Create pepper noise
pMask = imnoise(zeros(size(img)),'salt & pepper',0.0015);
% Apply the shape to each "grain"
pMask = conv2(pMask, shape,'same');
% Normalize the mask
pMask = min(pMask,1);
% Creating salt noise by inverting a new pepper noise mask
sMask = imnoise(zeros(size(img)),'salt & pepper',0.0015);
sMask = conv2(sMask, shape,'same');
sMask = min(sMask,1);
sMask = 1-sMask;
% Applying both masks to the original image
nImg = min(sMask,img);
nImg = max(pMask,nImg);
% Enhancing using the myMedian function
eImg = myMedian(nImg,1,7);
end