%201508793 Haberman Eran
%207424482 Kantor Adar
%
%The function tagConnectedComponents take a binary image and identify the
%   connected components in it with the Tag Connected Components algorithm.
%   the function then return a matrix (tagImg) represanting the components 
%   where every components tagged from 1 to n. the fuction also create a
%   colored image based on tags where every connected component colored in
%   a diffrent color (colorImg).
function [tagImg, colorImg] = tagConnectedComponents(img)
%initialize the argumants for the algorithm.
[rows, cols] = size(img);
tagImg = zeros(rows, cols);
nl = 1;
%initialize the adjacency pairs list.
adjPair = ones(0,2);

%tag the connected components based on their neighbors (top and left).
for r=1:rows
    for c=1:cols
        %ignore the background.
        if (img(r,c) == 0)
            continue;
        end
        %the cell has 2 neighbors.
        if (r > 1)&&(c > 1) && (img((r-1), c) == 1) && (img(r, (c-1)) == 1)
            tagImg(r,c) = tagImg((r-1), c);
            %set the 2 neighbors in the adjacency pair.
            adjPair = [adjPair; tagImg((r-1), c) tagImg(r, (c-1))];
        %the cell has 1 neighbor - up.
        elseif (r > 1) && (img((r-1), c) == 1)
            tagImg(r,c) = tagImg((r-1), c);
        %the cell has 1 neighbor - left.
        elseif (c > 1) && (img(r, (c-1)) == 1)
            tagImg(r,c) = tagImg(r , (c-1));
        %the cell has no neighbors and get a new tag.
        else
            tagImg(r,c) = nl;
            nl = nl+1;
        end
    end
end

%initialize the adjacency matrix with 1 in the i,i cells self adjacency.
nl = nl - 1;
adjMat = zeros(nl);
for i=1:nl
    adjMat(i,i)=1;
end

%set the adjacency pairs in the adjacency matrix.
adjCols = size(adjPair,1);
for i=1:adjCols
    adjMat(adjPair(i,1), adjPair(i,2)) = 1;
    adjMat(adjPair(i,2), adjPair(i,1)) = 1;
end

%build the rest of the adjacency matrix.
%we use the algorithm for multiplying matrices as we discuss in class.
for i=1:log(nl)
    OadjMat = adjMat;
    adjMat = adjMat * adjMat;
    adjMat = min(adjMat , 1);
    if (OadjMat == adjMat)
        break;
    end
end

%build the final adjacencies by giving each adjacency a final unified tag.
l = 2;
cv = adjMat(1,:);
for i=1:nl
    if (cv(i) == 0)
        cv = cv + (adjMat(i,:) * l);
        l = l + 1;
    end
end

%build tagImg acording to the tags in cv so each connected component has 
%it's own unified tag.
for r=1:rows
    for c=1:cols
        %ignore the background.
        if (tagImg(r,c) == 0)
            continue;
        end
        tagImg(r,c) = cv(tagImg(r,c));
    end
end

%build ccv a colorized version cv by randomize the values.
l = l - 1;
ccv = min(1, abs(randn(1,l,3)));
colorImg = zeros(rows,cols,3);

%build colorImg i.e. giving each connected component unique color.
for r=1:rows
    for c=1:cols
        %ignore the background.
        if (tagImg(r,c) == 0)
            continue;
        end
        colorImg(r,c,:) = ccv(1,tagImg(r,c),:);
    end
end
end