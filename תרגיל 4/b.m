clear;
clc;
    
imgs = [['balls1.tiff']; ['balls4.tiff']; ['balls5.tiff']];
for i = 2:2
    img = imread(imgs(i,:));
    img = double(img)/255;
    [circles,cImg] = findCircles(img);
    figure;imshow(cImg);
    fprintf('----\n\n')
end