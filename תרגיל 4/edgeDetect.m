%201508793 Haberman Eran
%207424482 Kantor Adar
%
% The function edgeDetect gets an img input and outputs an image with the
% edges being white and the rest black. Firstly it smooths the image with a
% gaussian filter and applies Sobel's edge detection. Then using N.M.S and
% Hysteresis thresholding the edges are of a single pixel width, continuous
% and not too many nor too little.
function newImg = edgeDetect (img)
% Apply Sobel edge detection
[mag, tang] = Sobel(img);
% Apply Non Maximum Supression
img = Suppression(img,tang,mag);
% Apply Hysteresis Thresholding
newImg = Threshold(img,0.35,0.6);
end

function [m,t] = Sobel(a)
% Use gaussian filter to reduce noise
a = conv2(a,[1,2,1;2,4,2;1,2,1]/16,'same');
% Get derivitive values for pixels by direction
x = conv2(a,[1,2,1;0,0,0;-1,-2,-1],'same');
y = conv2(a,[1,2,1;0,0,0;-1,-2,-1]','same');
% m - magnitude, t - tangent
t = a;
m = a;
for i = 1:numel(a)
    m(i) = (x(i)^2 + y(i)^2)^0.5;
    t(i) = y(i)/x(i);
end
end

function [e] = Suppression(a, tang, mag)
edge = a;
% Go through all pixels
for y = 2:size(mag,2)-1
    for x = 2:size(mag,1)-1
        % Check if this pixel is the maximum along the normal
        % Going through all tangent values for normals 
        if abs(tang(x,y)) > 2.4142
            if mag(x,y) == max([mag(x,y-1),mag(x,y),mag(x,y+1)])
                edge(x,y) = mag(x,y);
            else
                edge(x,y) = 0;
            end
        elseif abs(tang(x,y)) < 0.4142
            if mag(x,y) == max([mag(x-1,y),mag(x,y),mag(x+1,y)])
                edge(x,y) = mag(x,y);
            else
                edge(x,y) = 0;
            end
        elseif tang(x,y) > 0
            if mag(x,y) == max([mag(x-1,y-1),mag(x,y),mag(x+1,y+1)])
                edge(x,y) = mag(x,y);
            else
                edge(x,y) = 0;
            end
        else
            if mag(x,y) == max([mag(x-1,y+1),mag(x,y),mag(x+1,y-1)])
                edge(x,y) = mag(x,y);
            else
                edge(x,y) = 0;
            end
        end
    end
end
e = edge;
end

function [im] = Threshold(edge,high, low)
im = zeros(size(edge));
% Go through all pixels
for y = 2:size(im,2)-1
    for x = 2:size(im,1)-1
        % If the pixel is above high it is an edge
        if edge(x,y) > high
            im(x,y) = 1;
        % If the pixel is below low it is not an edge
        elseif edge(x,y) < low
            im(x,y) = 0;
        % If thepixel is between high and low it is and edge only if it has
        % an edge neighbor
        else
            im(x,y) = max(max(im(x-1:x+1,y-1:y+1)));
        end
    end
end
end