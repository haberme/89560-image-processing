%201508793 Haberman Eran
%207424482 Kantor Adar
%
%The function findCircles take an image and identify the circles in it
%   using the Canny Edge Detector (locate in edgeDetector.m).
%   the function will print the found circles and return
%   1)circles - matrix with the found circles as [Origin_x,Origin_y,Radius].
%   2)cImg - the given image with the circles marked on it.
%   @param img - The image on which the algorithm will detect the circles.
function [circles, cImg] = findCircles(img)
    %run the edge detection algorithm.
    edgeImg = edgeDetect(img);

    %initialize the argumants for the algorithm.
    [rows, cols] = size(edgeImg);
    rMax = min(rows, cols);
    count = zeros(cols, rows, rMax);

	%serch the edge image for cirlces using hough detection algorithm.
    for y=1:rows
        for x=1:cols
            %check if the current cell is edge of some element.
            if edgeImg(y, x) == 1
                for cY = 1:rows
                    for cX = 1:cols
                        %calculate the radius and check that it within the range.
                        r = round(sqrt((y - cY)^2 + (x - cX)^2));
                        if (r > 0) && (r <= rMax)
                            count(cX, cY, r)=count(cX, cY, r) + 1;
                        end
                    end
                end
            end
        end
    end

    %run the non maximum suppression algorithm to reduce wrong detected circles.
    circles = [];
    for y = 1:rows
        for x = 1:cols
            for r = rMax:-1:5
                %check that the detected circle is a circle.
                if isMax(count, cols, rows, x, y, r)
                    %found a cirlce, print and save it.
                    circles = [circles ;[x y r]];
                    fprintf('Circle %d: %d, %d, %d\n', size(circles, 1), x, y, r);
                end
            end
        end
    end

	%draw the circles on the image.
    cImg = img;
    for y = 1:rows
        for x = 1:cols
            for index = 1:size(circles, 1)
                if round(sqrt((circles(index, 1) - x)^2 + (circles(index, 2) - y)^2)) == circles(index, 3)
                    cImg(y, x) = 1;
                end
            end
        end
    end
end

%The function is Max take a point and a radius and check if the point is a
%   potential circle. the function check:
%   1)if the point is the maximum point, so that if the function encounter
%   an area of possible cirlcles coused by the hough detection algorithm
%   and the fact that the image might have a circle from a side making it a
%   little flat and not 100% round.
%   2)if the point pass a threshold meaning the the function really detect
%   a circle.
%   the function return true if the detected point is a circle.
%   @param count - The matrix with the detected circles points.
%   @param rows - The number of columns in the photo (check that we not go out of bound).
%   @param rows - The number of rows in the photo (check that we not go out of bound).
%   @param col - The column of the presumed circle.
%   @param row - The row of the presumed circle.
%   @param r - The radius of the presumed circle.
function bool = isMax (count, cols, rows, col, row, r)
    bool = false;
    
    %check the point against the threshold;
    %the threshold start big as circles with small redius tend to false
    %positive as they are small circles thus needed less points.
    %for balls5 turn 0.88 to 0.7.
    threshold = max(0.88, 2-(floor(r / 3) / 7)); 
    if count(col, row, r) < (threshold * pi * r)
        return;
        
	%chack if the point is the maximum in its area.
    else
        for i = -2:2
            nr = r + i;
            %check that the redius will not get us out of bound
            if (nr > 0) && (nr <= size(count, 3))
                for j = -1:1
                    for k = -1:1
                        nx = col + j;
                        ny = row + k;
                        %check if the point maximum and not out of bound.
                        if (nx > 0) && (nx <= cols) && (ny > 0) && (ny <= rows) && (count(col, row, r) < count(nx, ny, nr))
                            return;
                        end
                    end
                end 
            end
        end
    end

    %passed all the tests the point is a circle.
    bool = true;
end