function a
    clc;

    img4 = imread('fft4.tiff');
    img4 = double(img4)/255;
    %imshow(img4);

    f4 = fft2(img4);
    sf4 = fftshift(f4);
    %fftShow(sf4);

    sf4(293, 252) = -1500;
    sf4(307, 252) = -1500;
    sf4(300, 244) = -1500;
    sf4(300, 260) = -1500;
    %fftShow(sf4);

    isf4 = ifftshift(sf4);   
    cImg4 = ifft2(isf4);
    figure;imshow(cImg4);

    
end



function [ sImg ] = fftShow(img)
%   This function displays the given fft image

    sImg = log(abs(img)+1);
    sImg = sImg/max(max(sImg));
    imshow(sImg)

end