clear;
clc;
    
img1 = imread('fft1.tiff');
img1 = double(img1)/255;

img2 = imread('fft2.tiff');
img2 = double(img2)/255;

img3 = imread('fft3.tiff');
img3 = double(img3)/255;

img4 = imread('fft4.tiff');
img4 = double(img4)/255;

[cImg1,cImg2,cImg3,cImg4] = fftClean(img1,img2,img3,img4);

imshow(img1);
figure;imshow(cImg1);

figure;imshow(img2);
figure;imshow(cImg2);

figure;imshow(img3);
figure;imshow(cImg3);

figure;imshow(img4);
figure;imshow(cImg4);