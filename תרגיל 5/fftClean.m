%201508793 Haberman Eran
%207424482 Kantor Adar
%
%The function fftClean take the images fft1.tiff-fft4.tiff and clean them
%using fft.
%the function return the following:
%   1)cImg1 - cleaned version of fft1.tiff.
%   2)cImg2 - cleaned version of fft2.tiff.
%   3)cImg3 - cleaned version of fft3.tiff.
%   4)cImg4 - cleaned version of fft4.tiff.
%   @param img1 - fft1.tiff with the noise.
%   @param img2 - fft2.tiff with the noise.
%   @param img3 - fft3.tiff with the noise.
%   @param img4 - fft4.tiff with the noise.
function [cImg1,cImg2,cImg3,cImg4] = fftClean(img1,img2,img3,img4)
    %clean img1
    %img to fft.
    f1 = fft2(img1);
    sf1 = fftshift(f1);

    %eliminate the noise.
    sf1(267, 215) = 0;
    sf1(267, 227) = 0;

    %fft to img.
    isf1 = ifftshift(sf1);
    cImg1 = ifft2(isf1);
    
    %clean img2
    %img to fft.
    f2 = fft2(img2);
    sf2 = fftshift(f2);

    %eliminate the noise.
    sf2(232, 161) = 0;
    sf2(250, 171) = 0;

    %fft to img.
    isf2 = ifftshift(sf2);
    cImg2 = ifft2(isf2);
    
    %clean img3.
    %img to fft.
    f3 = fft2(img3);
    sf3 = fftshift(f3);

    %eliminate the noise.
    sf3(180, 113) = 0;
    sf3(180, 125) = 0;
    sf3(180, 137) = 0;
    sf3(190, 113) = 0;
    sf3(190, 137) = 0;
    sf3(200, 113) = 0;
    sf3(200, 125) = 0;
    sf3(200, 137) = 0;

    %fft to img.
    isf3 = ifftshift(sf3);
    cImg3 = ifft2(isf3);
    
    %clean img4.
    %img to fft.
    f4 = fft2(img4);
    sf4 = fftshift(f4);
    
    %eliminate the noise.
    %while we could use 0 to delete the distortion -1500 seem to eliminate
    %it better so we used it.
    sf4(293, 252) = -1500;
    sf4(307, 252) = -1500;
    sf4(300, 244) = -1500;
    sf4(300, 260) = -1500;

    %fft to img.
    isf4 = ifftshift(sf4);   
    cImg4 = ifft2(isf4);
end